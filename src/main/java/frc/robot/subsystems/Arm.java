package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.ArmConstants;

public class Arm extends SubsystemBase {
  private DoubleSolenoid doubleSolenoid = new DoubleSolenoid(ArmConstants.SOLENOID_0, ArmConstants.SOLENOID_1);
  private boolean extended;

  public Arm() {
    retract();
    extended = false;
  }

  public void extend() {
    doubleSolenoid.set(DoubleSolenoid.Value.kForward);
    extended = true;
  }

  public void retract() {
    doubleSolenoid.set(DoubleSolenoid.Value.kReverse);
    extended = false;
  }

  public void toggle() {
    if (extended) {
      retract();
    } else {
      extend();
    }
  }
}
