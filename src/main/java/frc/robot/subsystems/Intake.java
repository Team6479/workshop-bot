package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeConstants;

public class Intake extends SubsystemBase {
  private final TalonSRX motor = new TalonSRX(IntakeConstants.INTAKE_MOTOR);

  public Intake() {
    motor.configFactoryDefault();
    motor.setNeutralMode(NeutralMode.Coast);
    motor.setInverted(false);
  }

  public void stop() {
    motor.set(ControlMode.PercentOutput, 0);
  }

  public void run() {
    motor.set(ControlMode.PercentOutput, 0.5);
  }
}
