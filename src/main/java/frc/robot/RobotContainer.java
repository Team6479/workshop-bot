package frc.robot;

import com.team6479.lib.commands.TeleopTankDrive;
import com.team6479.lib.controllers.CBXboxController;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.Arm;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Intake;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  private final Drivetrain drivetrain = new Drivetrain();
  private final Intake intake = new Intake();
  private final Arm arm = new Arm();

  private final Compressor compressor = new Compressor(Constants.COMPRESSOR);

  private final CBXboxController xbox = new CBXboxController(0);
  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    drivetrain.setDefaultCommand(new TeleopTankDrive(drivetrain, () -> xbox.getY(Hand.kLeft), () -> xbox.getX(Hand.kRight)));

    xbox.getButton(XboxController.Button.kA)
        .whenPressed(new InstantCommand(intake::run, intake))
        .whenReleased(new InstantCommand(intake::stop, intake));
    // xbox.getButton(XboxController.Button.kA).whenPressed(new SequentialCommandGroup(
    //     new InstantCommand(intake::run, intake),
    //     new WaitCommand(5),
    //     new InstantCommand(intake::stop, intake)));

    xbox.getButton(XboxController.Button.kB)
        .whenPressed(new InstantCommand(arm::toggle, arm));
  }
}
