package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  public static final int COMPRESSOR = 0;

  public final class DrivetrainConstants {
    public static final int MOTOR_LEFT_FRONT = 0;
    public static final int MOTOR_LEFT_BACK = 1;
    public static final int MOTOR_RIGHT_FRONT = 2;
    public static final int MOTOR_RIGHT_BACK = 3;
  }

  public final class IntakeConstants {
    public static final int INTAKE_MOTOR = 4;
  }

  public final class ArmConstants {
    public static final int SOLENOID_0 = 0;
    public static final int SOLENOID_1 = 1;
  }
}
